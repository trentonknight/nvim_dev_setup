#i LUA configurations for NeoVim init.lua and associated files.

* [packer](https://github.com/wbthomason/packer.nvim)
* [neovim ide tutorial](https://mattermost.com/blog/turning-neovim-into-a-full-fledged-code-editor-with-lua/)

# Add Packer

```lua
vim.cmd [[packadd packer.nvim]]
return require('packer').startup(function(use)
use 'wbthomason/packer.nvim'
```

Additional plugins should be between the begining of the above function and completed with `end`

## Packer commands

-- You must run this or `PackerSync` whenever you make changes to your plugin configuration
-- Regenerate compiled loader file
:PackerCompile

-- Remove any disabled or unused plugins
:PackerClean

-- Clean, then install missing plugins
:PackerInstall

-- Clean, then update and install plugins
-- supports the `--preview` flag as an optional first argument to preview updates
:PackerUpdate

-- Perform `PackerUpdate` and then `PackerCompile`
-- supports the `--preview` flag as an optional first argument to preview updates
:PackerSync

-- Show list of installed plugins
:PackerStatus

-- Loads opt plugin immediately
:PackerLoad completion-nvim ale


```lua
end)
 ```

# Add Theme

* [tokyonight](https://github.com/folke/tokyonight.nvim)

```lua
use 'folke/tokyonight.nvim'
vim.cmd[[colorscheme tokyonight]]
```

# Add line numbers

```lua
vim.wo.number = true
```

# Install Mason

* [mason.nvim](https://github.com/williamboman/mason.nvim#configuration)

```lua
require("mason").setup()
use {
    "williamboman/mason.nvim",
    run = ":MasonUpdate" -- :MasonUpdate updates registry contents
}
```


