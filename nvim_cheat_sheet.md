# NeoVim Cheat Sheet for dev environment
Use the ! before standard cli commands from command mode. For example to quickly run yarn in the current directory.

```bash
:! yarn
```
You may have to use the $ key in nvim but the follow opens the terminal.

```bash
:term
```
## 

* [vim-netrw/](https://shapeshed.com/vim-netrw/)
For a full File Manager native netrw can be used.

netrw in current buffer
```bash
: Explore
```
netrw horizontal split
```bash
: Sexplore
```
netrw vertical split
```bash
: Vexplore 
```

# Buffers

Show open buffers

```bash
: ls
```
Select buffer by name

```bash
:b {bufname}
```
Close current buffer

```bash
:bd  
```
Switch to next buffer

```bash
:bn 
```
Switch to previous buffer

```bash
:bp 
```

Goto last buffer visited
```bash
:b#
```
Switch to buffer 1

```bash
:b1
```
Switch to next modified buffer

```bash
:bm
```


