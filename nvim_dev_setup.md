# NVIM init.vim file and vim-plug

## Dependancies first
* [https://github.com/nvm-sh/nvm](https://github.com/nvm-sh/nvm)

Install nvm

```bash
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
```
Then install node

```bash
nvm install node
```

```bash
pacman -Sy xdg-utils python 
```
Or using apt

```bash
sudo apt install python-is-python3 npm nodejs xdg-utils
```

## Rust language
Assuming RUST is not already installed:

```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

## Vim-plug

* [https://github.com/junegunn/vim-plug](https://github.com/junegunn/vim-plug)

```bash
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
```
### Create and edit init.vim for neovim

```bash
mkdir ~/.config/nvim/
touch ~/.config/nvim/init.vim
nvim ~/.config/nvim/init.vim
```

Now add the following:

```vim
" Install vim-plug, coc-nvim, nodejs before using the below
" ~/.config/nvim/init.vim

call plug#begin()
" The default plugin directory will be as follows:
"   - Vim (Linux/macOS): '~/.vim/plugged'
"   - Vim (Windows): '~/vimfiles/plugged'
"   - Neovim (Linux/macOS/Windows): stdpath('data') . '/plugged'
" You can specify a custom plugin directory by passing it as the argument
"   - e.g. `call plug#begin('~/.vim/plugged')`
"   - Avoid using standard Vim directory names like 'plugin'

" Make sure you use single quotes, place after plugin starts
Plug 'arcticicestudio/nord-vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'psf/black', { 'branch': 'stable' }
Plug 'neoclide/coc-tsserver'
Plug 'arcticicestudio/nord-vim'
Plug 'itchyny/lightline.vim'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'ayu-theme/ayu-vim'

set termguicolors
" let ayucolor="light"
let ayucolor="mirage" 
" let ayucolor="dark"  
" Set lightline themes
let g:lightline = {
      \ 'colorscheme': 'PaperColor',
      \ }

" Initialize plugin system, colorthemes must be selected AFTER
" Pluin calls end:

call plug#end()

" call themes after plugins, comment out unwanted theme
" Uncomment desired theme
"colorscheme ayu
colorscheme nord

let g:python_host_prog = 'usr/bin/python'
let g:python3_host_prog = '/usr/bin/python3'
let g:coc_node_path = '/usr/bin/node'    
```
# Yarn install for Conquer of Completion (COC)

* [https://yarnpkg.com/](https://yarnpkg.com/)

```bash
curl --compressed -o- -L https://yarnpkg.com/install.sh | bash
```
# Poetry install for Python package management.
* [https://python-poetry.org/docs/master/#installing-with-pipx](https://python-poetry.org/docs/master/#installing-with-pipx)

```bash
apt install pipx
```

# Black code style for Python

* [https://black.readthedocs.io/en/latest/getting_started.html](https://black.readthedocs.io/en/latest/getting_started.html)

```bash
pipx install git+https://github.com/psf/black
```

Black may be run in nvim, within python or via command line.

```bash
black {source_file_or_directory}...
```
Or
```python
python -m black {source_file_or_directory}...
```
Or in nvim
```vim
:Black
```

# ~/.bashrc setup

```bash
export PATH="~/.poetry/bin/:$PATH"
export PATH="~/.local/bin/:$PATH"
export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"
```

# Neoclide Conquer of Completion (COC)

Make your Neovim as smart as VSCode

* [https://github.com/neoclide/coc.nvim](https://github.com/neoclide/coc.nvim)
* [https://github.com/neoclide/coc.nvim/wiki/Install-coc.nvim](https://github.com/neoclide/coc.nvim/wiki/Install-coc.nvim)

Make sure both your `.bashrc` and `nvim.vim` have been edited as shown above or similar and vim.plug pluggins installed using:

```vim
:PlugInstall
```

Open neovim to check and see if the coc.nvim service is running:

```vim
:checkhealth
```
You can use the following command in Neovim to get useful info:

```vim
:CocInfo
```
For more details info look at the official [installation information](https://github.com/neoclide/coc.nvim/wiki/Install-coc.nvim).

## CoC extensions with npm

```bash
npm search coc
```


## Marketplace
To view CoC [extensions](https://github.com/neoclide/coc.nvim/wiki/Using-coc-extensions#manage-extensions-with-coclist) within vim install coc-marketplace.

```vim
:CocInstall coc-marketplace
```
Now search for desired extension.

```vim
:CocList marketplace python
```

## Optional COC Dependancies

### NPM

[NPM](https://www.npmjs.com/) with neovim:

```bash
sudo npm install -g [neovim](https://neovim.io/)
```
Install Rome for npm

```bash
sudo npm install -g rome@next
```


### Yarn

For [yarn](https://yarnpkg.com/) with neovim

```bash
yarn global add neovim
```
Install rome for yarn

```bash
yarn add -D rome@next
```

### Verify project root in your Javascript project root

* [package-json](https://docs.npmjs.com/cli/v6/configuring-npm/package-json)
* [Gulp quick-start/](https://gulpjs.com/docs/en/getting-started/quick-start/)
* [what-is-the-file-package-json/](https://nodejs.org/en/knowledge/getting-started/npm/what-is-the-file-package-json/)


Barebone project

```bash
npm init -y
```

`Wrote to /home/trentonknight/Development/Code-Platoon/Javascript/package.json:`

```json
{
  "name": "javascript",
  "version": "1.0.0",
  "description": "",
  "main": "back_iof_the_line.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC"
}
```
Now you can add needed scripts, in this example [rome](https://rome.tools/docs/introduction/getting-started):

```json
{
  "name": "javascript",
  "version": "1.0.0",
  "description": "",
  "main": "back_iof_the_line.js",
  "scripts": {
  "format": "rome format ."
  },
  "keywords": [],
  "author": "",
  "license": "ISC"
}
```
You may need to run yarn install to recognize changes in the new configuration:

```bash
yarn install
```
Now attempt to use yarn to call rome to format your javascript file via cli:

```bash
yarn rome format --write back_iof_the_line.js 
```


### Ruby

Install ruby

```bash
sudo apt install ruby ruby-dev
```
Install neovim gems

```bash
sudo gem install neovim
```
### Perl
* [coq_setup.md](https://gitlab.com/trentonknight/proofs_of_nothing/-/blob/master/coq_setup.md)

First install [Opam](https://opam.ocaml.org/doc/Install.html) and [OCaml](https://ocaml.org/docs/up-and-running):

```bash
bash -c "sh <(curl -fsSL https://raw.githubusercontent.com/ocaml/opam/master/shell/install.sh)"
```
#### Initialize opam

```bash
opam init
eval `opam env`
```
#### Use opam to install vcaml

```bash
opam install vcaml
```
## OCaml install

```bash
opam install dune utop ocaml-lsp-server merlin
```
### Neovim with Opam 
Add to ~/.config/nvim/init.vim

```vim
let g:opamshare = substitute(system('opam var share'),'\n$','','''')
execute "set rtp+=" . g:opamshare . "/merlin/vim"
```
Opam-user-setup support Merlin.

```bash
opam user-setup install
```
### CPan module Neovim::Ext

```bash
cpan install Neovim::Ext
```
## Rust Analyzer

* [https://rust-analyzer.github.io/manual.html](https://rust-analyzer.github.io/manual.html)

Assuming COC is running. Use the following to install the `coc-rust-analyzer`:
```vim
:CocInstall coc-rust-analyzer
```

# Markdown tools

* [coc-webview](https://github.com/weirongxu/coc-webview)
* [coc-markdown-preview-enhanced](https://github.com/weirongxu/coc-markdown-preview-enhanced)

To allow markdown web based preview with nvim install the following with COC:

```vim
:CocInstall coc-webview
```
Now install coc-markdown-preview-enhanced:

```vim
:CocInstall coc-markdown-preview-enhanced
```
Now while editing Markdown in nvim use the following:
```vim                                                                                    128,0-1        Bot
:CocCommand markdown-preview-enhanced.openPreview
```
# Javascript setup

The below is for [coc](https://github.com/neoclide/coc.nvim/wiki/Install-coc.nvim).

```bash
:CocInstall coc-json
```
### ESlint
* [https://github.com/neoclide/coc.nvim/wiki/Using-the-configuration-file](https://github.com/neoclide/coc.nvim/wiki/Using-the-configuration-file)
* [https://github.com/neoclide/coc-eslint](https://github.com/neoclide/coc-eslint)

```bash
:CocInstall coc-eslint
```

Edit coc-settings config here: `~/.config/nvim/coc-settings.json`

```json
{  
        "eslint.enable": true,                                                                             
        "eslint.packageManager": "yarn",                               
        "eslint.autoFixOnSave": true,  
        "eslint.run": "onSave",  
        "eslint.debug": true,  
        "eslint.format.enable": true   
}
```
# Typescript
* [https://github.com/neoclide/coc-tsserver](https://github.com/neoclide/coc-tsserver)
* [https://github.com/neoclide/coc.nvim/wiki/Using-the-configuration-file](https://github.com/neoclide/coc.nvim/wiki/Using-the-configuration-file)

```bash
CocInstall tsserver
```
Now add tssever to your `~/.config/nvim/coc-settings.json` as previously shown above with eslint:

```json
{  
        "eslint.enable": true,  
        "eslint.packageManager": "yarn",  
        "eslint.autoFixOnSave": true,  
        "eslint.run": "onSave",  
        "eslint.debug": true,  
        "eslint.format.enable": true,  
        "markdown-preview-enhanced.previewTheme": "vue.css",  
        "tsserver.enable": true  
}
```

### Rome and Node quick examples

Rome for formating and node for debugging cli with example js file:

```bash
rome format --write back_iof_the_line.js 
```


```bash
node --inspect back_iof_the_line.js 
```



# github gist

coc-gist will request a token creation and allow authentication to [gist.github.com](https://gist.github.com).

```vim
:CocInstall coc-gist
```
# Fern and Nerdfonts

## Nerdfont Install

```bash
git clone --depth 1 https://github.com/ryanoasis/nerd-fonts.git
cd nerd-fonts
./install.sh
ls ~/.local/share/fonts/NerdFonts/
```

### Append to init.vim:

```bash
Plug 'lambdalisue/fern.vim'
Plug 'lambdalisue/nerdfont.vim'
Plug 'lambdalisue/fern-renderer-nerdfont.vim'
let g:fern#renderer = "nerdfont"
let g:fern#default_hidden = 1
let mapleader=","
nmap <leader>f :Fern . -drawer -toggle<CR>
```

### Alacritty terminal font

Ensure you have ~/.alacritty.yml. If not pull the latest version:

```bash
curl -O https://raw.githubusercontent.com/alacritty/alacritty/master/alacritty.yml
```
Now ensure it is a hidden file within your home directory:

```bash
mv alacritty.yml .alacritty.yml
```

Append the following to alacritty config in `.alacritty.yml`

```yml
# Font configuration
font:
  # Normal (roman) font face
  normal:
    # Font family
    #
    # Default:
    #   - (macOS) Menlo
    #   - (Linux/BSD) monospace
    #   - (Windows) Consolas
    #family: "monospace"
    family: "JetBrainsMono Nerd Font Mono"

    # The `style` can be specified to pick a specific face.
    #style: Thin
```

Enter directory that includes some good files that might use Devicons styles and open neovim. Then use the following command in nvim:

```
:Fern .
```
Or hit `,` + f using the mapped keys you created above to use the drawer toggle function, hit a second time to close.

# Moving to Fern window and back

This is actually a native neovim CMD. All Windows cammand work with CRTL+W. Therefore, switching windows and in this case the Fern drawer just use CRTL+W and the appropriate arrow key. Better yet use :help wincmd to find the latest features or.

* [https://neovim.io/doc/user/windows.html](https://neovim.io/doc/user/windows.html)
